Implementierung einer IIIF-Schnittstelle für die Digitalisate der FAUUB
=======================================================================

Aufbauend auf den Erfahrungen aus dem Prototyp beschreibt dieses Dokument,
wie eine IIIF-Schnittstelle für (Teile der) Digitalisate der UB aussehen könnte
und  welche Schritte noch nötig sind, um den Prototypen in eine produktive Schnittstelle
weiter zu entwickeln.


Umfang des IIIF-Dienstes
------------------------

Der IIIF-Dienst zerfällt in zwei Teildienste, die jeweils die IIIF-Standards Image API beziehungsweise Presentation API implementieren.

Bei der Image API handelt es sich um einen Dienst, der Bilder beziehungsweise deren Derivate über standardisierte URLs ausliefert. Zur Umsetzung wird die Software Cantaloupe benutzt.

Bei der Presentation API handelt es sich um einen Dienst, der Metadaten zu einer Bildserie (hier: Scans der Seiten eines Werks) in Form eines sog. Manifests als Datei bereitstellt. Die Presentation API baut auf der Image API auf. Die Umsetzung ist eine Eigenentwicklung.


Abhängigkeit von Digitool und B3Kat
-----------------------------------

Die IIIF-Schnittstelle greift auf Schnittstellen von Digitool zurück, um Bilder sowie Metadaten zu Bildern und Serien abzufragen.
Die SRU-Schnittstelle des B3Kat wird für die Abfrage von Werkmetadaten benötigt. 
Bei den Digitool-Schnittstellen handelt es sich vermutlich um inoffizielle Web-Schnittstellen --- eventuell sollte mit der VZ geklärt werden, ob dies problematisch ist und ob andere Schnittstellen zu bevorzugen wären.

Der Prototyp verfolgt die Strategie, bei jeder Anfrage die benötigten Daten von den Schnittstellen zu holen und on-the-fly das Derivat beziehungsweise Manifest zu generieren.
Im Produktivbetrieb würde diese Lösung die Schnittstellen übermäßig belasten bei nur schwacher Performanz für die NutzerInnen.
(Insbesondere das Generieren des Manifests erzeugt viele Anfragen an Digitool, da alle Bilder der Serie angefragt werden müssen.)
Für den Produktivbetrieb müssen Anfragen und Transformationsergebnisse zwischengespeichert werden. 
Hier sind verschiedene Szenarien vorstellbar.
Es wird folgendes Vorgehen vorgeschlagen:

Die Manifeste (Bildserienmetadaten) werden vorab periodisch generiert und vorgehalten.
Bilder, auch zur Erzeugung von Derivaten, werden live geholt.
Sollte nach Tests die Performanz nicht ausreichen, so werden auch die Bilder stärker lokal gecached. Dies kann auch noch geschehen, wenn sich erst im Produktivbetrieb Engpässe ergeben.

Die Vor- und Nachteile gegenüber der Implementierung im Prototypen sind:
- Vorteile: Schnelligkeit; Stabilität; Beanspruchung von Digitool niedriger und planbar; höhere Auflösung bei Bildern möglich
- Nachteile: Hoher Speicherverbrauch, va. wenn Bilder vorrätig gehalten werden; Datendoppelung; Zugriffspeeks auf Digitool bei Generierung; Propagieren von Änderungen/Veraltete Daten


Schritte 
--------
|    | Schritt                                   | geschätzte Personenstunden
|:---|:------------------------------------------|---------:
|1.  |VM-Server beantragen und einrichten        | 2h(?)
|2.  |Bildserver Cantaloupe aufsetzen            | 1-10h
|3.  |Eigenes Skript weiterentwickeln und härten | ~10-40h
|4.  |Persistenten Webdienst einrichten          | 5h
|5.  |Testen und Korrektur                       | 20h
|6.  |Daten bereitstellen (evtl.)                | 5h
|7.  |Integration in Digitool-Viewer             |2h(?)+VZ
		
### Erläuterungen
1.	Server beantragen und einrichten: Dedizierten, weltweit zugreifbaren Server aufsetzen und Domain beantragen.
Zwar könnte der Dienst auch auf einem bestehenden Server mitlaufen, zur besseren Kapselung ist ein eigener Server aber vorzuziehen.
Ausstattung: Die Anwendungen sowie die Manifest-Dateien benötigen nicht nennenswert Festplattenplatz. Sofern Bilder vorgehalten werden, ist vermutlich das Mounten eines Netzlaufwerks (X) die beste Lösung. (s.a. Schritt 2)
2.	Bildserver Cantaloupe aufsetzen: Cantaloupe implementiert die IIIF Image API. Auf Source-Bilder kann sowohl lokal als auch über das Netz zugegriffen werden. Derivate können on-the-fly erzeugt werden. Es gibt Caching-Mechanismen. Installation und Konfiguration sind relativ einfach.
Ob das Holen und Verarbeiten on-the-fly performant genug ist, muss noch getestet werden.
Bei Bedarf lassen sich die Einstellungen auch relativ leicht nachträglich ändern und über eine API kann nötigenfalls Verhalten überschrieben werden.
Systemanforderungen: https://cantaloupe-project.github.io/manual/5.0/deployment.html
3.	Eigenes Skript weiterentwickeln und härten: Das Skript ist verantwortlich für das Zusammentragen der Metadaten für die IIIF-Manifeste (Metadaten zu Bildserien). Es muss vom prototypischen zu einem produktiven Zustand weiterentwickelt werden. Insbesondere müssen Fehlerfälle erkannt und abgefangen werden. Das Vorhalten einmal generierter Manifeste muss implementiert werden.
Optionale Erweiterungen:
  a. Ausweitung der aus MARC übernommenen, mitgelieferten menschenlesbaren Werksmetadaten 
  b. Genauere Abbildung der Inhaltsstruktur
  c. Unterstützung von Annotationen wie Transkriptionen
4.	Persistenten Webdienst einrichten: Sowohl Cantaloupe als auch das eigenentwickelte Skript sollten nicht direkt im Web exponiert werden, sondern hinter einen Webserver (Apache, nginx, …) geschaltet werden, der http und https bedient. Alle Dienste müssen so eingerichtet sein, dass sie nach einem Absturz oder Systemneustart automatisch neu starten.
5.	Testen und Korrektur: Test auf Fehler und Performanz. Wie oben beschrieben muss eventuell die Strategie zum Vorhalten der Daten geändert werden.
6.	Daten bereitstellen: Dieser Schritt fällt nur an, wenn Daten manuell erzeugt werden müssen, um vorgehalten zu werden.
7.	Integration in Digitool-Viewer: Denkbar wäre das Aufführen der Manifest-URL in der Metadaten-Section im Viewer. Was wie umgesetzt werden kann, muss mit der VZ (Frau Schröder) noch eruiert werden.


Anhang: Prototyp (Mailtext)
---------------------------

Liebe Kolleginnen und Kollegen,

das Thema IIIF kommt immer wieder hoch und insbesondere die Frage, ob und
wie eine IIIF-konforme Auslieferung unserer Digitalisate umsetzbar ist.

Um in der Diskussion etwas voranzukommen und Wege aufzuzeigen, habe ich eine
IIIF-Schnittstelle erstellt, die als Wrapper um Digitool fungiert:
Für die Erzeugung der IIIF-Daten werden (fast) ausschließlich
digitool-Schnittstellen angezapft.
Die Implementierung ist aktuell ein reiner Proof-of-concept, dh. nicht
stabil und in dieser Form nicht produktiv einsetzbar! Sie soll einfach
zeigen, dass eine IIIF-Unterstützung
aus technischer Sicht relativ leicht umzusetzen wäre -- als "Add-on" zu
Digitool. (Das Ganze ist nicht mit der VZ besprochen und es ist möglich,
dass dort ein solches Add-on -- auch aus technischen Gründen -- kritisch
gesehen wird.)

In einem Video erkläre ich die Funktionsweise der Schnittstelle und wie man
sie selbst ausprobieren kann. [^1]
Da sie momentan auf dem DHLab-Server läuft, ist sie allerdings nur aus dem
Uni-Netz/VPN erreichbar.

Die darin angesprochenen URLs sind unten nochmal aufgeführt, darunter für
Mirador [^2], einem Beispiel für die Cantaloupe-Schnittstelle [^3] sowie
einigen Beispielen für die IIIF-Manifest-Schnittstelle[^4] [^5] [^6]. Die
Studon-OER zu IIIF und Mirador befindet sich unter [^7].

Fragen -- auch bei Problemen mit der Schnittstelle -- gerne an mich.

Viele Grüße
Martin Scholz

[^1]: Erklärvideo: http://dhlab.ub.fau.de/files/iiif-proof-of-concept.mkv
[^2]: Mirador: https://projectmirador.org/
[^3]: Cantaloupe-Beispiel: http://dhlab.ub.fau.de:8182/iiif/2/14161400/full/full/0/default.jpg
[^4]: IIIF-Manifest-Beispiel: http://dhlab.ub.fau.de:27666/14161399/manifest
[^5]: IIIF-Manifest-Beispiel: http://dhlab.ub.fau.de:27666/14161317/manifest
[^6]: IIIF-Manifest-Beispiel: http://dhlab.ub.fau.de:27666/11015195/manifest (~600 Bilder, Generierung kann dauern!)
[^7]: OER: https://www.studon.fau.de/pg569030_2993840.html


API-Beschreibung
----------------

### URL/URI-Patterns

Mit * sind URLs/URIs gekennzeichnet, die auch tatsächlich abrufbar sind.
Mit ? sind solche gekennzeichnet, die noch nicht implementiert sind beziehungsweise optional implementiert werden könnten.

Image API:
- */image/<image pid>/info.json

Presentation API:
- */presentation/<work pid>/manifest
- /presentation/<work pid>/sequence/<seq id>
- /presentation/<work pid>/canvas/<canvas id> 
- /presentation/<work pid>/image/<image pid> für die Hauptbilder
- *!/presentation/<work pid>/annotation/<anno id> für Annos, die keine Hauptbilder sind
- !/presentation/<work pid>/toc

### Metadaten-Felder
-	Catalog-Number: BV-Nummer
-	Shelfmark: Signatur
-	Author: Author, ermittelt aus MARC Feld 100??a
-	Place: Erscheinungsort, ermittelt aus MARC Feld 264??a
-	Date: Erscheinungsdatum, ermittelt aus MARC Feld 264??c
