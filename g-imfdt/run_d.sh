#!/bin/bash

cd `dirname $0`
# encapsulate both commands so we get the right pid
nohup bash -c "npm start; echo $! > nohup.pid" > nohup.out 2>&1 &

