/* **
 *
 * G-imfdt -- Generate iiif manifest from digitool
 *
 * Code style: https://firefox-source-docs.mozilla.org/code-quality/coding-style/coding_style_js.html
 *
 * **/


/* **
 * CONFIGURATION
 * **/

/* 
 * These constants can be set be environment variables; but default is a reasonable value
 */
// if the env is not defined, we use https 
const USE_HTTPS   = (typeof process.env.USE_HTTPS !== 'undefined') && !(/^(false|no|0)$/.test(process.env.USE_HTTPS));
const PORT        = process.env.PORT       || 27666;
const PORT_HTTPS  = process.env.PORT_HTTPS || 27661;
const CRED_PRIV   = process.env.CRED_PRIV;
const CRED_CERT   = process.env.CRED_CERT;
const DATA_DIR    = process.env.DATA_DIR   || '/var/www/html/iiif'; // no trailing slash!

/*
 * URLs for fetching data; should normally not be changed
 */
const URL_DIGITOOL_STRUCTMAP = "http://digital.bib-bvb.de/view/bvbmets/getStructMap.jsp?pid=${pid}&parta=DE-29&au=BAY01"
const URL_INFO_JSON          = "http://dhlab.ub.fau.de:8182/iiif/2/${pid}/info.json";
const URL_MIDMAP_BY_PID      = "http://dhlab.ub.fau.de:28777/pid/${pid}";
const URL_B3KAT_SRU          = "http://bvbr.bib-bvb.de:5661/bvb01sru?version=1.1&recordSchema=marcxml&operation=searchRetrieve&maximumRecords=1&query=marcxml.idn%3D%22${bvnr}%22"

const DATA_SUBDIR_MANIFESTS = 'manifest'; // no leading and trailing slashes!

/*
 * Constants for manifest files
 */
const URI_PREFIX = "http://dhlab.ub.fau.de:27666/";


/*
 * Namespaces for processed docs; cannot and should not be configured
 */
const namespaces = {
  "marc": "http://www.loc.gov/MARC21/slim",
  "zs": "http://www.loc.gov/zing/srw/"
};


/* **
 * IMPORTS / REQUIRED MODULES
 * **/

const express   = require('express');
const fs        = require('fs');
const fetch     = require('node-fetch');
const DOMParser = require('xmldom').DOMParser;
const xpath     = require('xpath').useNamespaces(namespaces);


/* **
 * SERVER SETUP
 * **/
const app = express();
var http = require('http');
var serverHttp = http.createServer(app);
var serverHttps = null;
if (USE_HTTPS) {
  var https = require('https');
  var privateKey  = fs.readFileSync(CRED_PRIV, 'utf8');
  var certificate = fs.readFileSync(CRED_CERT, 'utf8');
  var credentials = {key: privateKey, cert: certificate};
  serverHttps = https.createServer(credentials, app);
}
console.log('Using HTTP' + (USE_HTTPS ? 'S' : ''));



/* **
 * ROUTING FUNCTIONS
 * **/

/* Routing table:
 * /:pid/manifest              GET       Manifest by pid
 */

/** Handler for adding CORS support
 *
 * see also IIIF manifest store: https://github.com/textandbytes/iiif-manifest-store
 */
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    
  // intercept OPTIONS method: it merely asks for the CORS info in header
  // => skip body
  if ('OPTIONS' == req.method) {
    res.sendStatus(200);
  }
  else {
    next();
  }
});


/** Route handler for requests by BV number 
 *
 */
app.route('/:pid/manifest').get(function(req, res, next) {
  var timestamp = Date.now();
  app.checkAccessPid(req.params.pid)
    .then(() => app.generateManifest(req.params.pid))
    .then(manifest => Promise.all([
      res.json(manifest),
      app.saveToFile(manifest)
    ]))
    .then(
      () => console.log("processed request for pid " + req.params.pid + " in " + (Date.now() - timestamp) + " millisecs"),
      err => next(err)
    );
});


/* **
 * HELPER FUNCTIONS
 * **/

/** Replace placeholders of the form ${placeholder} in a string
 *
 * If ${placeholder} is not in map, the ${} is discarded but the placeholder is not substituted
 */
app.substitutePlaceholders = function(str, map) {
  return str.replace(/\$\{(\w+)\}/g, (match, p1) => { return map[p1] || p1; });
};


/** Perform checks on Pid whether we allow access
 *
 */
app.checkAccessPid = function(pid) {
  return Promise.resolve(true);  
};


/** Generate a iiif manifest for the given pid
 *
 */
app.generateManifest = function(pid) {
  if (!/^\d+$/.test(pid)) {
    throw new Error("PID must consist of digits only; given " + pid);
  }
  // we build a manifest stub and fetch data from different web services to 
  // complete the manifest
  console.log("Got manifest request for pid " + pid);
  return app.getManifestStub(pid) 
    .then(manifest => app.insertWorkAndItemMetadata(manifest))
    .then(manifest => app.insertStructMap(manifest))
    .then(manifest => app.insertImageMetadata(manifest));
};


/** Generate the basic structure of the manifest and fill in static/immutable data
 *
 */
app.getManifestStub = function(pid) {
  return Promise.resolve({
    // temporary information
    "_pid": pid,  
    // Metadata about this manifest file
    "@context": "http://iiif.io/api/presentation/2/context.json",
    "@id": URI_PREFIX + pid + "/manifest",
    "@type": "sc:Manifest",
     // Rights Information
    "license": "https://creativecommons.org/licenses/by-sa/3.0/",
    "attribution": "Universitätsbibliothek Erlangen-Nürnberg",
    // Presentation Information
    "viewingDirection": "right-to-left",
    "viewingHint": "paged",
    "metadata": [],
    "sequences": [
      {
        "@id": URI_PREFIX + pid + "/sequence/1",
        "@type": "sc:Sequence",
        "label": "Folge",
        "viewingDirection": "left-to-right",
        "viewingHint": "paged",
        "startCanvas": URI_PREFIX + pid + "/canvas/1",
        "canvases": []
      }
    ]
  });
}


/** Fetch other identifiers (BV number, shelfmark) and bibliographic metadata
 *
 */
app.insertWorkAndItemMetadata = function(manifest) {
  return fetch(app.substitutePlaceholders(URL_MIDMAP_BY_PID, {'pid': manifest._pid}))
    .then(result => result.json())
    .then(midmap => {
      manifest._bvnr = midmap[0].bv;
      manifest._shelfmark = midmap[0].shm;
      manifest.metadata = manifest.metadata || [];
      manifest.metadata.push({
        "label": "Shelfmark",
        "value": manifest._shelfmark
      });
      manifest.metadata.push({
        "label": "Catalog number (BV-Nummer)",
        "value": manifest._bvnr
      });
      console.log("object is also identified by " + manifest._bvnr + " / " + manifest._shelfmark);
      return manifest;
    })
    .then(manifest => {
      return fetch(app.substitutePlaceholders(URL_B3KAT_SRU, {'bvnr': manifest._bvnr}))
        .then(result => result.text())
/*        .then(text => { console.log("My text:", text, "url:",app.substitutePlaceholders(URL_B3KAT_SRU, {'bvnr': manifest._bvnr})); return manifest; } )*/
        .then(xml => (new DOMParser()).parseFromString(xml))
        .then(dom => {
          // der Haupttitel
          manifest.label = xpath("string(//marc:datafield[@tag='245']/marc:subfield[@code='a'])", dom);
          // weitere Metadaten
          manifest.metadata.push({
            "label": "Author",
            "value": xpath("string(//marc:datafield[@tag='100']/marc:subfield[@code='a'])", dom)
          });
          manifest.metadata.push({
            "label": "Place",
            "value": xpath("string(//marc:datafield[@tag='264']/marc:subfield[@code='a'])", dom)
          });
          manifest.metadata.push({
            "label": "Date",
            "value": xpath("string(//marc:datafield[@tag='264']/marc:subfield[@code='c'])", dom)
          });
          console.log("added metadata");
          return manifest;
        });
    })
};


/** get the sequence of pages and their metadata 
 *
 */
app.insertStructMap = function(manifest) {
  return fetch(app.substitutePlaceholders(URL_DIGITOOL_STRUCTMAP, {'pid': manifest._pid}))
    .then(result => result.json())
    .then(structMap => app.parseStructMap(manifest, structMap[0]))
    .then(manifest => { console.log("created structMap with " + manifest.sequences[0].canvases.length + " canvases"); return manifest; });
};


app.parseStructMap = function(manifest, structMap) {
  if (structMap.attr && structMap.attr.pid) {
    var canvas = {
      "@id": URI_PREFIX + manifest._pid + "/canvas/" + (manifest.sequences[0].canvases.length + 1),
      "@type": "sc:Canvas",
      "label": structMap.data || "",
      "images": [
        {
          "_pid": structMap.attr.pid,
          "@id": URI_PREFIX + manifest._pid + "/image/" + structMap.attr.pid,
          "@type": "oa:Annotation",
          "motivation": "sc:painting",
          "resource": {
            "@id": "http://dhlab.ub.fau.de:8182/iiif/2/" + structMap.attr.pid + "/full/full/0/default.jpg",
            "@type": "dctypes:Image",
            "format": "image/jpeg",
            "service": {
              "@context": "http://iiif.io/api/image/2/context.json",
              "@id": "http://dhlab.ub.fau.de:8182/iiif/2/" + structMap.attr.pid,
              "profile": "http://iiif.io/api/image/2/level2.json"
            },
          },
          "on": URI_PREFIX + manifest._pid + "/canvas/" + (manifest.sequences[0].canvases.length + 1)
        }
      ]
    };
    manifest.sequences[0].canvases.push(canvas);
  }
  if (structMap.children) {
    structMap.children.forEach(child => app.parseStructMap(manifest, child));
  }
  return manifest;
};


/** Insert IIIF image info
 *
 */
app.insertImageMetadata = function(manifest, index) {
  index = index || 0;
  if (manifest.sequences[0].canvases.length <= index) {
    return manifest;
  }
  else {
    var canvas = manifest.sequences[0].canvases[index];
    return fetch(app.substitutePlaceholders(URL_INFO_JSON, {'pid': canvas.images[0]._pid }))
      .then(result => result.json())
      .then(info => {
        // we only need image dimension and we have to attach them to both canvas and image
        //canvas.height = canvas.images[0].resource.height = info.height;
        //canvas.width =  canvas.images[0].resource.width  = info.width;
        manifest.sequences[0].canvases[index].height = manifest.sequences[0].canvases[index].images[0].resource.height = info.height;
        manifest.sequences[0].canvases[index].width =  manifest.sequences[0].canvases[index].images[0].resource.width  = info.width;
        // do loggging
        if (index % 10 == 9) {
          console.log("finished inserting metadata for " + (index + 1) + " images");
        }
        // go to next index
        return app.insertImageMetadata(manifest, index + 1);
      });
  }
};


/** Write generated manifest file to a file as a cache
 *
 */
app.saveToFile = function(manifest) {
  // cf. https://stackoverflow.com/a/35211152
  return new Promise(function(resolve, reject) {
    fs.access(DATA_DIR + '/' + DATA_SUBDIR_MANIFESTS, fs.constants.W_OK, function (err1) {
      if (err1) {
        console.log("cannot access manifest data dir " + DATA_DIR + '/' + DATA_SUBDIR_MANIFESTS, err1);
        reject(err1);
      }
      else {
        fs.writeFile(DATA_DIR + '/' + DATA_SUBDIR_MANIFESTS + '/' + manifest._pid + '.json', JSON.stringify(manifest), function (err2) {
          if (err2) {
            console.log("cannot write manifest to file " + DATA_DIR + '/' + DATA_SUBDIR_MANIFESTS + '/' + manifest._pid + '.json:', err1);
            reject(err2);
          }
          else {
            console.log("wrote manifest to file " + DATA_DIR + '/' + DATA_SUBDIR_MANIFESTS + '/' + manifest._pid + '.json');
            resolve();
          }
        });
      }
    });
  });
};


/* **
 * START SERVER
 * **/
serverHttp.listen(PORT, function () {
  console.log('G-imfdt started listening on port ' + PORT);
});
if (serverHttps) {
  serverHttps.listen(PORT_HTTPS, function () {
    console.log('G-imfdt started listening on port ' + PORT_HTTPS + " for access with https");
  });
}

