G-Imfdt --- Generate IIIF Manifests from Digitool
=================================================

Small service that generates a IIIF manifest for a given Aleph PID.


Used ID patterns
----------------

Patterns are taken from Presentation API 2.0 recommendation.
Patterns marked with * may be dereferenced.

- */presentation/<work pid>/manifest
- /presentation/<work pid>/sequence/<seq id>
- /presentation/<work pid>/canvas/<canvas id> 
- /presentation/<work pid>/image/<image pid>

Metadata fields
---------------

-	Catalog-Number: BV-Nummer
-	Shelfmark: Signatur
-	Author: Author, taken from MARC 100??a
-	Place: publication place, taken from MARC 264??a
-	Date: publication date, taken from MARC 264??c
